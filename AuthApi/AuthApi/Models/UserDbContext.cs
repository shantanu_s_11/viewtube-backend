﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthApi.Models
{
    public class UserDbContext 
    {
        MongoClient client;
        IMongoDatabase database;
        public UserDbContext()
        {
            client = new MongoClient("mongodb://localhost:27017");
            database = client.GetDatabase("UserDB");
        }
        public IMongoCollection<User> Users => database.GetCollection<User>("Users");
    }
}

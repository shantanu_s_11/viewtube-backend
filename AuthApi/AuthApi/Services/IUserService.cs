﻿using AuthApi.Models;
using System.Collections.Generic;

namespace AuthApi.Services
{
    public interface IUserService
    {
        bool Login(User user);
        bool RegisterUser(User user);
        List<User> GetAllUsers();
    }
}
﻿using AuthApi.Exceptions;
using AuthApi.Models;
using AuthApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthApi.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _repo;
        public UserService(IUserRepository repo)
        {
            _repo = repo;
        }
        public bool Login(User user)
        {
            var _user = _repo.Login(user);
            if(_user == true)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public bool RegisterUser(User user)
        {
            var _user = _repo.FindUserById(user.UserId);
            if (_user==null)
            {
                var insert = _repo.Register(user);
                if (insert == true)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }

        }
        public List<User> GetAllUsers()
        {
            return _repo.ShowUsers();
        }
    }
}

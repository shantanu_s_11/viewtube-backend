﻿using AuthApi.Models;
using System.Collections.Generic;

namespace AuthApi.Repository
{
    public interface IUserRepository
    {
        User FindUserById(string UserId);
        bool Login(User user);
        bool Register(User user);
        List<User> ShowUsers();
    }
}
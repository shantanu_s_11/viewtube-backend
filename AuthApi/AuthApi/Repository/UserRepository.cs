﻿using AuthApi.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthApi.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly UserDbContext _context;
        public UserRepository(UserDbContext context)
        {
            _context = context;
        }
        public User FindUserById(string UserId)
        {
            var p = _context.Users.Find(x => x.UserId == UserId).FirstOrDefault();
            return p;
        }

        public bool Login(User user)
        {
            var u = _context.Users.Find(x => x.UserId == user.UserId && x.Password == user.Password).FirstOrDefault();
            if(u!=null)
            {
                return true;
            }
            else
            {
                return false;
            }    
        }
        public bool Register(User user)
        {
            _context.Users.InsertOne(user);
            return true;
        }
        public List<User> ShowUsers()
        {
            return _context.Users.Find(x => true).ToList();
        }
    }
}

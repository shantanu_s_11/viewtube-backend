﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AuthApi.Exceptions;
using AuthApi.Models;
using AuthApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Bson.IO;

namespace AuthApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IUserService _service;
        public AuthController(IUserService service)
        {
            _service = service;
        }
        [EnableCors("Policy1")]
        [HttpPost]
        [Route("register")]
        public IActionResult Register([FromBody] User user)
        {
            bool insert=_service.RegisterUser(user);
            if (insert == true)
                return StatusCode(201, "You are successfully Registered!");
            else
                return StatusCode(500, "User already Exists");  
        }
        [EnableCors("AnotherPolicy")]
        [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody] User user)
        {
            bool _user = _service.Login(user);
            if (_user == true)
            {
                return Ok(GetToken(user.UserId));
            }
            else
            {
                return StatusCode(500,"Invalid username or password!!");
            } 
        }
        [HttpGet]
        [Route("show")]
        public List<User> GetUser()
        {
            return _service.GetAllUsers();
        }
        private string GetToken(string userId)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName,userId),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("Authserver_secret_to_validate_token"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: "AuthServer",
                audience: "jwtclient",
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(20),
                signingCredentials: creds
            );
            var response = new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token)
            };
            return Newtonsoft.Json.JsonConvert.SerializeObject(response);
        }
    }
}

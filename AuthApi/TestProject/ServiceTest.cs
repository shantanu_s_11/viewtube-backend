using AuthApi.Controllers;
using AuthApi.Models;
using AuthApi.Repository;
using AuthApi.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using Xunit;

namespace TestProject
{
    public class ServiceTest
    {
        [Fact]
        public static void LoginReturnTrue()
        {
            var mockRepo = new Mock<IUserRepository>();
            User user = new User();

            mockRepo.Setup(repo => repo.Login(user)).Returns(true);
            var service = new UserService(mockRepo.Object);

            var actual = service.Login(user);
            Assert.True(actual);
        }
        [Fact]
        public static void RegisterUserReturnTrue()
        {
            var mockRepo = new Mock<IUserRepository>();
            User user = new User();

            mockRepo.Setup(repo => repo.Register(user)).Returns(true);
            var service = new UserService(mockRepo.Object);

            var actual = service.RegisterUser(user);
            Assert.True(actual);
        }
    }
}
